from django.contrib import admin
from .models import City,Flat,Booking

# Register your models here.


class BookingFlatInLine (admin.TabularInline):
    model = Booking
    max_num = Booking.objects.all().count()+1




class FlatAdmin(admin.ModelAdmin):
    list_display = ('name','occupied',)
    list_filter = ('occupied',)
    inlines = [BookingFlatInLine]




class CityAdmin(admin.ModelAdmin):
    list_display = ('names',)
    show_change_link = True

    def names(self,obj):
        #TODO change url depended from object
        return '<a href="/admin/rentFlat/flat/">flats</a>'

    names.allow_tags = True


admin.site.register(City,CityAdmin)
admin.site.register(Flat,FlatAdmin)
