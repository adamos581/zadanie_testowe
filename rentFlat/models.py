from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from django.db.models import Q


from django.conf import settings
# Create your models here.

class City(models.Model):

    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name




class Flat(models.Model):

    name = models.CharField(max_length=50)
    city = models.ForeignKey(City,related_name='flats')
    street = models.CharField(max_length=75)
    postal_code = models.CharField(max_length=5)
    occupied = models.BooleanField(default=False)
    booked = models.ManyToManyField(User, through='Booking')

    def __str__(self):
        return self.name


class Booking(models.Model):
    person = models.ForeignKey(User)
    flat = models.ForeignKey(Flat)
    created = models.DateTimeField(auto_now_add=True)
    start_rent=models.DateTimeField()
    finish_rent=models.DateTimeField()


    def save(self, *args, **kwargs):
        try:
            Booking.objects.get(
            Q(start_rent__range=(self.start_rent, self.finish_rent)) | Q(finish_rent__range=(self.start_rent, self.finish_rent)) )
        except Booking.DoesNotExist:

            super(Booking, self).save(*args, **kwargs)





